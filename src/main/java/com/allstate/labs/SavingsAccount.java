package com.allstate.labs;

public class SavingsAccount extends Account {

    public SavingsAccount(double balance, String name) {
        super(balance, name);
    }

	@Override
    public void addInterest() {
        setBalance(getBalance() * 1.1);
    }
}
