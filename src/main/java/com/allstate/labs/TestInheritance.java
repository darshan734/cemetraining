package com.allstate.labs;

public class TestInheritance {
    public static void main(String[] args) {
        Account accnt[] = new Account[3];

         accnt[0] = new Account();
         accnt[1] = new SavingsAccount(10000, "Rahul");
         accnt[2] = new CurrentAccount(10000, "Shreya");
         accnt[0].addInterest();
         accnt[1].addInterest();
         accnt[2].addInterest();
    }
}
