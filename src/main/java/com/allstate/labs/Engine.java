package com.allstate.labs;

public interface Engine {
    double getEngineSize();
    void setEngineSize(double engineSize);
}
