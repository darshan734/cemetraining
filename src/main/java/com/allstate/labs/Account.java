package com.allstate.labs;

public class Account {
    private double balance;
    private String name;
    private final static int interestRate = 10;

    public Account() {
        this.balance = 50;
        this.name = "Darshan";
    }

    public static int getInterestrate() {
        return interestRate;
    }

    public Account(double balance, String name) {
        this.balance = balance;
        this.name= name;
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void addInterest() {
       this.balance = this.balance + (this.balance/100.0) * interestRate;
        System.out.printf("Name : %s, Balance : %f", this.name, this.balance);
    }

    public boolean withDraw(double amount) {
        return this.checkBalance(amount);
    }

    public boolean withDraw() {
        return this.checkBalance(20);
    }

    private boolean checkBalance(double amount) {
        if(amount <= this.balance) {
            this.balance = this.balance - amount;
            return true;
        } else {
            return false;
        }
}
}
