package com.allstate.entities;

import org.springframework.data.annotation.Id;

public class Person {
    @Id
    private int id;
    private String name;
    private int age;
    //quiz, final has 3 uses , constant, sealing (cannot subclass), cannot override a final method
    private final static int agelimit;
    
static{
    agelimit=105;
}

//default
public Person()
{
   // agelimit=105;
    this.id=0;
}
public Person(int id)
{
    this.id=id;
}



public static int getAgeLimit()
{
    return agelimit;
}

    public int getId()
    {
        return this.id;
    }
    public void setId(int id)
    {
        this.id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

   
// public void display()
// {
//         System.out.printf("Values are %s,  %d" , this.name, this.id);
// }

protected  void print()
{
        System.out.printf("Printed Values are %s,  %d" , this.name, this.id);
}


public static void  printf()
{         
    System.out.printf("PERSON: Formatted Printed Values");
}


public Person(int id, String name, int age) {
    this.id = id;
    this.name = name;
    this.age = age;
}
    
}
