package com.allstate.di;

import org.springframework.stereotype.Component;


public interface Pet {
    void feed();
}
