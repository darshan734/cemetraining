package com.allstate.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.allstate.labs")
public class PetConfiguration {
    // @Bean
    // public Owner owner(@Autowired Pet pet) {
    //     return new Owner(pet);
    // }

    // @Bean
    // public Pet pet() {
    //     return new Dog();
    // }
}
