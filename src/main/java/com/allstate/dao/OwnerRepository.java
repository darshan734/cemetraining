package com.allstate.dao;

import com.allstate.di.Owner;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OwnerRepository extends MongoRepository<Owner, ObjectId>{
    
}
