
package com.allstate;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.allstate.dao.Db;
import com.allstate.entities.Employee;
import com.allstate.service.EmployeeService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@ExtendWith(SpringExtension.class)
//@TestInstance(Lifecycle.PER_CLASS)
@ContextConfiguration(classes = MongoJavaConfig.class, loader = AnnotationConfigContextLoader.class)
public class EmployeeServiceTest {
    //Mock
@Autowired
private EmployeeService service;

@Test
public void testSaveEmployee()
{
    Employee e1 = new Employee(112, "dee", 22, "d:i.ie", 2000);
    service.save(e1);
    assertTrue( service.Total()>0);
  
}

}
